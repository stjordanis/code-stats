defmodule CodeStats.User.CacheUtils do
  @moduledoc """
  Utilities for user's cache generation and updating.
  """

  require Logger

  import Ecto.Query, only: [from: 2]

  alias Ecto.Changeset
  alias CodeStats.{Repo, Utils, XP}
  alias CodeStats.User.{Pulse, Cache, Flow}

  @doc """
  Calculate and store cached XP values for user, using all historical data to reset the cache.
  """
  @spec update_all!(User.t()) :: :ok
  def update_all!(user) do
    # Don't use any previous cache data
    empty_cache = %{}

    # Load all of user's XP plus required associations
    xps =
      from(
        x in XP,
        join: p in Pulse,
        on: p.id == x.pulse_id,
        where: p.user_id == ^user.id and not is_nil(x.language_id),
        select: {p, x},
        order_by: [asc: p.sent_at]
      )
      |> Repo.all()
      |> case do
        nil -> []
        ret -> ret
      end

    operation = fn cache ->
      Enum.reduce(xps, cache, fn {pulse, xp}, acc ->
        update_cache_from_xp(
          acc,
          pulse.sent_at,
          try_sent_at_local(pulse),
          xp.language_id,
          pulse.machine_id,
          xp.amount
        )
      end)
    end

    cache = update(empty_cache, operation, true)

    # Persist cache changes and update user's last cached timestamp
    {:ok, _} =
      Changeset.cast(user, %{cache: cache}, [:cache])
      |> Changeset.put_change(:last_cached, DateTime.utc_now() |> DateTime.truncate(:second))
      |> Repo.update()

    :ok
  end

  @doc """
  Update given user cache (from DB) with the updater operation, set the caching duration, and then
  return the data in DB format for persisting back.
  """
  @spec update(Cache.db_t() | nil, (Cache.t() -> Cache.t()), boolean) :: Cache.db_t()
  def update(cache, operation, is_total)

  # Protect from nil user cache (new user, first pulse?)
  def update(nil, operation, is_total), do: update(%{}, operation, is_total)

  def update(cache, operation, is_total)
      when is_map(cache) and is_function(operation, 1) and is_boolean(is_total) do
    update_start_time = DateTime.utc_now()
    duration_key = if is_total, do: "total_caching_duration", else: "caching_duration"

    unformat_cache_from_db(cache)
    |> operation.()
    |> format_cache_for_db()
    |> Map.put(duration_key, get_caching_duration(update_start_time))
  end

  @doc """
  Update user's cache with the given details and list of XPs. Returns the updated cache.
  """
  @spec update_cache_from_xps(
          Cache.t(),
          DateTime.t(),
          NaiveDateTime.t() | DateTime.t(),
          integer,
          [
            %{language_id: integer, amount: integer}
          ]
        ) :: Cache.t()
  def update_cache_from_xps(%Cache{} = cache, datetime, local_datetime, machine_id, xps)
      when is_integer(machine_id) and is_list(xps) do
    Enum.reduce(
      xps,
      cache,
      &update_cache_from_xp(&2, datetime, local_datetime, &1.language_id, machine_id, &1.amount)
    )
  end

  @doc """
  Update user's cache with the given details. Returns the updated cache.
  """
  @spec update_cache_from_xp(
          Cache.t(),
          DateTime.t(),
          NaiveDateTime.t() | DateTime.t(),
          pos_integer(),
          pos_integer(),
          integer()
        ) :: Cache.t()
  def update_cache_from_xp(
        %Cache{} = cache,
        datetime,
        local_datetime,
        language_id,
        machine_id,
        amount
      )
      when is_integer(language_id) and is_integer(machine_id) and is_integer(amount) do
    # Timestamp could be a DateTime (instead of NaiveDateTime) for old XPs
    date = local_datetime.__struct__.to_date(local_datetime)

    languages = Map.update(cache.languages, language_id, amount, &(&1 + amount))
    machines = Map.update(cache.machines, machine_id, amount, &(&1 + amount))
    dates = Map.update(cache.dates, date, amount, &(&1 + amount))
    hours = Map.update(cache.hours, local_datetime.hour, amount, &(&1 + amount))

    {flows, provisional_flow} = update_flows(cache, datetime, local_datetime, language_id, amount)

    %Cache{
      cache
      | languages: languages,
        machines: machines,
        dates: dates,
        hours: hours,
        flows: flows,
        provisional_flow: provisional_flow
    }
  end

  @doc """
  Unformat data from DB to native datatypes. If cache is nil, treat it as empty.

  ## Examples

      iex> CodeStats.User.CacheUtils.unformat_cache_from_db(%{
      ...>   "languages" => %{"1" => 2, "15" => 3},
      ...>   "machines" => %{"2" => 5},
      ...>   "dates" => %{"2019-05-05" => 5},
      ...>   "caching_duration" => 5.4,
      ...>   "flows" => [%{"s" => "2019-02-05T20:00:01Z", "sl" => "2019-02-05T22:00:01", "d" => 25, "l" => %{"5" => 15}, "x" => 15}]
      ...> })
      %CodeStats.User.Cache{
        languages: %{1 => 2, 15 => 3},
        machines: %{2 => 5},
        dates: %{~D[2019-05-05] => 5},
        hours: %{},
        caching_duration: 5.4,
        total_caching_duration: 0.0,
        flows: [
          %CodeStats.User.Flow{
            start_time: ~U[2019-02-05T20:00:01Z],
            start_time_local: ~N[2019-02-05T22:00:01],
            duration: 25,
            languages: %{5 => 15},
            xp: 15
          }
        ]
      }

      iex> CodeStats.User.CacheUtils.unformat_cache_from_db(nil)
      %CodeStats.User.Cache{
        languages: %{},
        machines: %{},
        dates: %{},
        hours: %{},
        caching_duration: 0.0,
        total_caching_duration: 0.0,
        flows: []
      }
  """
  @spec unformat_cache_from_db(Cache.db_t() | nil) :: Cache.t()
  def unformat_cache_from_db(cache)

  def unformat_cache_from_db(cache) when is_map(cache) do
    languages =
      Map.get(cache, "languages", %{})
      |> Utils.str_keys_to_int()

    machines =
      Map.get(cache, "machines", %{})
      |> Utils.str_keys_to_int()

    dates =
      Map.get(cache, "dates", %{})
      |> Map.to_list()
      |> Enum.map(fn {key, value} -> {Date.from_iso8601!(key), value} end)
      |> Map.new()

    hours =
      Map.get(cache, "hours", %{})
      |> Map.to_list()
      |> Enum.map(fn {key, value} -> {String.to_integer(key), value} end)
      |> Map.new()

    flows =
      Map.get(cache, "flows", [])
      |> Enum.reduce([], fn cache_flow, acc ->
        case unformat_flow(cache_flow) do
          :invalid ->
            acc

          {:ok, flow} ->
            [flow | acc]
        end
      end)
      |> Enum.reverse()

    provisional_flow =
      with data when not is_nil(data) <- Map.get(cache, "provisional_flow"),
           {:ok, f} <- unformat_flow(data) do
        f
      else
        _ -> nil
      end

    %Cache{
      languages: languages,
      machines: machines,
      dates: dates,
      hours: hours,
      flows: flows,
      provisional_flow: provisional_flow,
      caching_duration: Map.get(cache, "caching_duration", 0.0),
      total_caching_duration: Map.get(cache, "total_caching_duration", 0.0)
    }
  end

  # The cache may be nil, for a new user for example
  def unformat_cache_from_db(nil), do: %Cache{}

  @doc """
  Given a single flow in cache format, transform it into a Flow struct and return it.
  """
  @spec unformat_flow(Flow.cache_t()) :: {:ok, Flow.t()} | :invalid
  def unformat_flow(cache_data) do
    with {:ok, timestamp} <- Map.fetch(cache_data, Flow.cache_key(:start_time)),
         {:ok, timestamp_local} <- Map.fetch(cache_data, Flow.cache_key(:start_time_local)),
         {:ok, duration} <- Map.fetch(cache_data, Flow.cache_key(:duration)),
         {:ok, amount} <- Map.fetch(cache_data, Flow.cache_key(:amount)),
         {:ok, languages} <- Map.fetch(cache_data, Flow.cache_key(:languages)),
         {:ok, start_time, 0} <- DateTime.from_iso8601(timestamp),
         {:ok, start_time_local} <- NaiveDateTime.from_iso8601(timestamp_local) do
      {:ok,
       %Flow{
         start_time: start_time,
         start_time_local: start_time_local,
         duration: duration,
         xp: amount,
         languages: Utils.str_keys_to_int(languages)
       }}
    else
      _ -> :invalid
    end
  end

  # Format data in cache for storing into DB as JSON
  @spec format_cache_for_db(Cache.t()) :: Cache.db_t()
  defp format_cache_for_db(%Cache{} = cache) do
    languages = Utils.int_keys_to_str(cache.languages)

    machines = Utils.int_keys_to_str(cache.machines)

    dates =
      cache.dates
      |> Map.to_list()
      |> Enum.map(fn {key, value} -> {Date.to_iso8601(key), value} end)
      |> Map.new()

    hours =
      cache.hours
      |> Map.to_list()
      |> Enum.map(fn {key, value} -> {Integer.to_string(key), value} end)
      |> Map.new()

    flows = cache.flows |> Enum.map(&format_flow/1)

    provisional_flow =
      case cache.provisional_flow do
        nil -> nil
        flow -> format_flow(flow)
      end

    %{
      "languages" => languages,
      "machines" => machines,
      "dates" => dates,
      "hours" => hours,
      "flows" => flows,
      "provisional_flow" => provisional_flow,
      "caching_duration" => cache.caching_duration,
      "total_caching_duration" => cache.total_caching_duration
    }
  end

  @spec update_flows(
          Cache.t(),
          DateTime.t(),
          NaiveDateTime.t() | DateTime.t(),
          pos_integer(),
          integer()
        ) ::
          {[Flow.t()], Flow.t() | nil}
  defp update_flows(%Cache{} = cache, datetime, local_datetime, language_id, amount) do
    {provisional_flow, flows} =
      cond do
        not is_nil(cache.provisional_flow) ->
          {cache.provisional_flow, cache.flows}

        cache.flows != [] ->
          [first | rest] = cache.flows
          {first, rest}

        true ->
          {nil, []}
      end

    if is_nil(provisional_flow) do
      {[],
       %Flow{
         start_time: datetime,
         start_time_local: local_datetime,
         xp: amount,
         languages: %{language_id => amount}
       }}
    else
      last_time =
        DateTime.add(provisional_flow.start_time, provisional_flow.duration * 60, :second)

      diff = DateTime.diff(datetime, last_time)

      cond do
        diff < 0 ->
          # Given XP is earlier than latest/provisional flow, ignore it. It will be caught by next
          # full cache refresh.
          {cache.flows, cache.provisional_flow}

        diff <= Flow.max_pause() * 60 ->
          updated_languages =
            Map.update(provisional_flow.languages, language_id, amount, &(&1 + amount))

          provisional_flow = %Flow{
            provisional_flow
            | duration: provisional_flow.duration + round(diff / 60),
              xp: provisional_flow.xp + amount,
              languages: updated_languages
          }

          if provisional_flow.duration >= Flow.min_length() do
            {[provisional_flow | flows], nil}
          else
            {flows, provisional_flow}
          end

        true ->
          {cache.flows,
           %Flow{
             start_time: datetime,
             start_time_local: local_datetime,
             xp: amount,
             languages: %{language_id => amount}
           }}
      end
    end
  end

  @spec get_caching_duration(DateTime.t()) :: float
  defp get_caching_duration(start_time) do
    Calendar.DateTime.diff(DateTime.utc_now(), start_time)
    |> (fn {:ok, s, us, _} -> s + us / 1_000_000 end).()
  end

  # Try using local sent_at time if available, fall back on more inaccurate sent_at
  @spec try_sent_at_local(Pulse.t()) :: DateTime.t() | NaiveDateTime.t()
  defp try_sent_at_local(%Pulse{} = pulse) do
    case pulse.sent_at_local do
      %NaiveDateTime{} = dt -> dt
      nil -> pulse.sent_at
    end
  end

  @spec format_flow(Flow.t()) :: Flow.cache_t()
  defp format_flow(%Flow{} = flow) do
    %{
      Flow.cache_key(:start_time) => DateTime.to_iso8601(flow.start_time),
      Flow.cache_key(:start_time_local) => NaiveDateTime.to_iso8601(flow.start_time_local),
      Flow.cache_key(:duration) => flow.duration,
      Flow.cache_key(:amount) => flow.xp,
      Flow.cache_key(:languages) => Utils.int_keys_to_str(flow.languages)
    }
  end
end
