import { el } from 'redom';
import { DateTime } from 'luxon';

import { XP_FORMATTER } from '../../../common/xp_utils';
import { format_mins, format_date } from '../../../common/utils';

class TopFlowsHighlightComponent {
  constructor(description) {
    this.valEl = el('p.top-flows-highlight-val');
    this.timeEl = el('time.top-flows-highlight-time');

    this.el = el('section.top-flows-highlight', [
      el('h5.top-flows-highlight-description', description),
      this.valEl,
      this.timeEl,
    ]);
  }

  update(val, date) {
    this.valEl.textContent = val;
    this.timeEl.textContent = format_date(date);
    this.timeEl.setAttribute('time', date.toISODate());
  }
}

class TopFlowsComponent {
  constructor() {
    this.heading = el('h4.top-flows-heading', 'Top flows');
    this.longestEl = new TopFlowsHighlightComponent('Longest');
    this.mostProlificEl = new TopFlowsHighlightComponent('Most XP');
    this.strongestEl = new TopFlowsHighlightComponent('Most XP/min');

    this.el = el('section.top-flows', [
      this.heading,
      this.longestEl,
      this.mostProlificEl,
      this.strongestEl,
    ]);

    this._machines = [];
  }

  setInitData({ top_flows }) {
    const longestDate = DateTime.fromISO(top_flows['longest']['startTimeLocal'], { setZone: true });
    const mostProlificDate = DateTime.fromISO(top_flows['mostProlific']['startTimeLocal'], { setZone: true });
    const strongestDate = DateTime.fromISO(top_flows['strongest']['startTimeLocal'], { setZone: true });

    this.longestEl.update(format_mins(top_flows['longest']['duration']), longestDate);
    this.mostProlificEl.update(XP_FORMATTER.format(top_flows['mostProlific']['xp']), mostProlificDate);
    this.strongestEl.update(XP_FORMATTER.format(top_flows['strongest']['xp'] / top_flows['strongest']['duration']), strongestDate);
  }

  update() { }
}

export { TopFlowsComponent };
