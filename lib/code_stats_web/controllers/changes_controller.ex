defmodule CodeStatsWeb.ChangesController do
  use CodeStatsWeb, :controller
  import Phoenix.HTML, only: [sigil_E: 2]

  @spec changes(Plug.Conn.t(), map()) :: Plug.Conn.t()
  def changes(conn, _params) do
    conn
    |> assign(:title, "Changes")
    |> assign(:change_data, change_data(conn))
    |> render("changes.html")
  end

  @spec change_data(Plug.Conn.t()) :: [{String.t(), Date.t(), String.t(), [String.t()]}]
  defp change_data(conn) do
    [
      {"2.2.0", ~D[2021-01-03], "New year, new beginnings",
       [
         "Added Gravatar support for profile pages.",
         "Added support for user specific language aliases.",
         "Added code-stats-browser to plugins page.",
         "Updated privacy policy to refer to Tmi AhlCode.",
         "Only suggest Mozilla Firefox as modern browser to IE users.",
         "Added support page with instructions how to support the project.",
         "By default only show 10 \"other languages\" in profile.",
         "Added more loading indicators and removed some that were too flashy.",
         "Change all future XP over 1,000 to 1 XP to prevent plugin issues.",
         "Made password reset form case insensitive.",
         "Added more AppSignal instrumentation.",
         "Changed data export timeout to be longer.",
         "Made it possible to export user data from legal terms consent page.",
         "Plenty of other fixes.",
         "Bumped versions of libraries and Elixir.",
         "Upgraded to PostgreSQL 12."
       ]},
      {"2.1.0", ~D[2019-09-09], "New server",
       [
         "Migrated service to a new server, so legal terms were updated.",
         "Added hourly bar chart. Thanks for the implementation, Serafeim Papastefanos!"
       ]},
      {"2.0.12", ~D[2019-05-16], "I want to get off Mr. Fixes' wild ride",
       [
         "Fix password reset saying it didn't work, even though it did.",
         "Fix deletion of user account in legal terms view not working."
       ]},
      {"2.0.11", ~D[2019-04-05], "The fixes never end",
       [
         "Fix crash in GraphQL when new user doesn't have cache.",
         "Fix crash in week languages graph due to missing update.",
         "Add longer reconnect timeout to Phoenix channel connections.",
         "Update tzdata version to fix crash with new tzinfo."
       ]},
      {"2.0.10", ~D[2019-03-31], "Cache update fix",
       [
         "Fix cache update not setting last updated date and stalling on same users."
       ]},
      {"2.0.9", ~D[2019-03-28], "More fixes",
       [
         "Fix password reset not rendering errors properly.",
         "Fix crash in profile data when user has nil cache."
       ]},
      {"2.0.8", ~D[2019-03-22], "Quick fixes",
       [
         "Fixed bugs in machine editing.",
         "Fixed frontpage graphs not updating.",
         "Made XP history cache update less often due to server load."
       ]},
      {"2.0.7", ~D[2019-03-21], "Transactionality",
       [
         "Made pulse adding work inside a transaction to prevent dangling DB rows in case of problems.",
         "Added many tests and data quality checks.",
         "Refactored code in several forms.",
         "Added contributors and VB6 plugin information.",
         "Updated logo."
       ]},
      {"2.0.6", ~D[2018-12-25], "🐛💥",
       [
         "Fixed a bug in profile GraphQL queries."
       ]},
      {"2.0.5", ~D[2018-12-09], "Cache generation fix",
       [
         "Generate user caches throughout the day instead of all in one block. This lessens the server load.",
         "Bump versions: Elixir to 1.7,  Phoenix to 1.4, Ecto to 3, Distillery to 2, and many others.",
         "Added Discord and Matrix links to footer."
       ]},
      {"2.0.4", ~D[2018-07-11], "GeoIP fix",
       [
         "Fix corrupted GeoIP databases, thereby fixing frontpage world map graph.",
         "Fix Phoenix cache manifest issue."
       ]},
      {"2.0.3", ~D[2018-07-07], "Even more fixes",
       [
         "Fix issues with year XP table and highlight current day.",
         "Fix link to VSCode extension on plugins page.",
         "Fix wrong data in languages list due to not resetting data when reinitting.",
         "Hour of day graph improvements."
       ]},
      {"2.0.2", ~D[2018-07-03], "More fixes",
       [
         "Fixed off-by-one error hiding one language in languages list.",
         "Don't send geoip coordinates to frontpage for private profiles.",
         "Fix frontpage heading sizes not to clip on mobile."
       ]},
      {"2.0.1", ~D[2018-07-01], "Email fix",
       [
         "Fixed crash when trying to send password reset for a user with no email.",
         "Removed unnecessary data from front page channel push."
       ]},
      {"2.0.0", ~D[2018-07-01], "Brand spanking new",
       [
         "Totally new UI written with CSS grid layout.",
         ~E(New graphs on profile pages and front page written with <a href="https://redom.js.org/" target="_blank">RE:DOM</a>.),
         "Major upgrades to code base in order to make it more maintainable and smaller releases easier.",
         ~E(<a href="https://github.com/bitwalker/distillery" target="_blank">Distillery</a> releases for great good.),
         "Data export feature for XP and private data.",
         "Crashers and other bugs fixed.",
         "Terms of service acceptance dialog with diffs to old terms of services.",
         "New GraphQL API for fetching XP data, token auth support coming later.",
         "Shiny contributors list on this page.",
         "Added all implemented plugins to plugins page.",
         "Added mass mailing system for mailing about big future updates.",
         "Removed Google Analytics in favor of Tilastokeskus.",
         "Dropped Internet Explorer support.",
         "And lots more."
       ]},
      {"1.8.6", ~D[2017-10-02], "AppSignal filtering",
       [
         "Filtered out some data that doesn't need to go to AppSignal."
       ]},
      {"1.8.5", ~D[2017-10-01], "AppSignal &amp; token expiration",
       [
         "Added AppSignal integration for metrics and error reports to aid in development.",
         "Added token expiration to channels so that you need to refresh the page at least once every 3 months."
       ]},
      {"1.8.4", ~D[2017-10-01], "Updated to Elixir 1.5, bumped deps",
       [
         "So I kind of blew up the thing accidentally while restarting it. Now it is fixed."
       ]},
      {"1.8.3", ~D[2017-07-16], "Small fixes",
       [
         ~S(Fixed the "private profile" checkbox not working properly in the preferences page.),
         "Added support for CORS into the user profile read API."
       ]},
      {"1.8.2", ~D[2017-04-09], "Groundwork for future refactoring",
       [
         "Added the storing of the user's local timestamp and timezone offset when creating pulses. This will be used for better graphs in the future.",
         "Added note about the VSCode plugin to the plugins page."
       ]},
      {"1.8.1", ~D[2017-01-18], "Fixes + Sublime Text 3 support",
       [
         "Bumped Elixir version to 1.4 and fixed compilation warnings.",
         "Fix decimals being printed wrong with Elixir 1.4.",
         "Added information about new Sublime Text 3 plugin.",
         "Added editor logos to plugin page."
       ]},
      {"1.8.0", ~D[2017-01-02], "Language aliases",
       [
         ~S[Added a language alias feature. Now some languages are just aliases for others. This is done to fix typos and problems in language detection in editors. For example, "JavaScript 1.5" (which was released in 2000 and is erroneously detected by JetBrains in some situations) will now be corrected to "JavaScript" and all existing XP has been combined between the languages. If you have feedback or suggestions on languages to merge, give us feedback on our Gitter or IRC channel.],
         "Added information about Gitter channel into footer, removed useless contact page.",
         ~E[Added user profile API for fetching data of public users. Integrate Code::Stats with your website! Check the <a href="<%= Routes.page_path(conn, :api_docs) %>">API documentation</a> for more information.]
       ]},
      {"1.7.2", ~D[2016-09-12], "Machine API key and caching duration stuff",
       [
         "Fixed machine API key changing when machine name was changed.",
         "Added caching duration to user's cache to monitor how long generating caches takes. ⏱",
         "Fixed duplicate update query when updating user's cache."
       ]},
      {"1.7.1", ~D[2016-09-03], "Small fixes to live updates",
       [
         "Fixed machine ordering on profile page being wrong when live updating.",
         "Fixed live updates not working on IE and other silly browsers by adding Babel polyfill."
       ]},
      {"1.7.0", ~D[2016-08-25], "Live updates ✨",
       [
         ~S[Added live update functionality to profile page and index page. XP numbers and progress bars update live using Elm. 📊 Index page includes a ticker that shows users gaining XP live (private profiles are shown as "Private user").],
         "Bumped dependencies. Update of Plug results in a different format for API tokens, but the old ones will still work."
       ]},
      {"1.6.2", ~D[2016-08-08], "Small fixes",
       [
         "Remove redundant ARIA tags from progress bars to show more descriptive textual info for screen readers.",
         "Fix error message printed in console when trying to access a page that does not allow authentication, while being logged in."
       ]},
      {"1.6.1", ~D[2016-08-06], "Fixes to machines and page permissions",
       [
         "Forbid authenticated users from accessing the login, signup and password reset pages. They will be redirected to their profile instead.",
         "Fix crash when machine was deleted, caused by the cache not being regenerated. Now the cache is regenerated automatically after delete and crash is avoided even if regeneration is still in progress.",
         "Add a JavaScript confirmation prompt when deleting a machine."
       ]},
      {"1.6.0", ~D[2016-08-04], "Password resets and fixes",
       [
         "Added password reset feature. Password can only be reset if the account has an email address set up.",
         "Updated privacy policy and terms of service due to email sending feature.",
         "Fixed 💥 when forms like the signup form were filled with invalid data.",
         "Fixed crash when pulses were sent before caches were generated.",
         "Fixed crash when pulses were sent with the API token in an invalid format.",
         "Added Twitter link.",
         "Added Elixir and Phoenix link to footer.",
         "Fixed discrepancies in the API docs regarding pulse dates.",
         ~S(An issue with the "last coded at" date in 1.5.0 was hotfixed.)
       ]},
      {"1.5.0", ~D[2016-07-17], "Second performance update",
       [
         "Earlier, profile view XPs per language were cached to the database as CachedXP elements. This worked fine for that purpose but caching for other dimensions such as per machine and per date were needed. In this update, CachedXP is removed in favor of a JSON field on the user model, which can be used to add all kinds of different caches. Caching is implemented in this field for languages, machines and dates. This removes the need to hit the database for other uses than to update the cache and to load the last 12 hours' XP data. As a result the profile view's display should be much faster.",
         "An issue with 1.4.0 and database limits in queries was hotfixed."
       ]},
      {"1.4.0", ~D[2016-07-16], "First performance update",
       [
         "Total XP and most popular languages counters on the front page are now in-memory, stored inside ETS. They are updated on every incoming pulse (for now) and totally refreshed every 15 minutes. This should make the front page much faster.",
         ~S("Added an index to make "XP in last 12 hours" queries faster both on front page and profile pages.),
         "Accessibility was improved with regards to dates and progress bars.",
         "Fixed average XP per day display, which showed XP in scientific format in some cases.",
         "Added real page titles for better SEO and bookmarking.",
         "Updated the codebase for Elixir 1.3, Phoenix 1.2 and Ecto 2.0.",
         "Fixed some JSON errors not being sent correctly in the API."
       ]},
      {"1.3.7", ~D[2016-06-08], "Fix username uniqueness",
       [
         "Fix username uniqueness, due to a problem with SQL migrations a unique index was not created and multiple users could be registered with the same username. 😱",
         "Change some IDs to bigint because if it's worth doing, it's worth overdoing.",
         "Remove old unused total XP property from users.",
         "Clarified git repo licences a bit."
       ]},
      {"1.3.3–1.3.6", ~D[2016-06-07], "JetBrains and username path fixes",
       [
         "Added details of the published IntelliJ/JetBrains plugin.",
         "Fixed spaces in usernames resulting in unreachable profile pages.",
         "Disallowed the plus sign in usernames to avoid conflicts with spaces."
       ]},
      {"1.3.2", ~D[2016-06-06], "JSON errors",
       [
         "Enabled JSON errors for easier debugging of 500 Internal server errors."
       ]},
      {"1.3.1", ~D[2016-06-05], "Language name fix",
       [
         "Fixed languages being created with different capitalisations. Now language names are case insensitive."
       ]},
      {"1.3.0", ~D[2016-06-04], "Machine stats",
       [
         "Added machine statistics to profile page.",
         "Added recalculation of XP once a day to fix XP errors in profile view.",
         "Added favicons.",
         "Added more user info to profile view."
       ]},
      {"1.2.2", ~D[2016-06-02], "Vincit Oy",
       [
         "Added note about my company who have sponsored the development of the service.",
         "Added more information to the plugins page."
       ]},
      {"1.2.1", ~D[2016-05-31], "Small fixes",
       [
         ~E(Fix <a href="https://github.com/Nicd/code-stats/issues/1">#1</a>: coded_at time range not documented in API docs),
         ~E(Fix <a href="https://github.com/Nicd/code-stats/issues/2">#2</a>: Endpoint path typo in API docs),
         ~E(Fix <a href="https://github.com/Nicd/code-stats/issues/4">#4</a>: Refactor README),
         ~E(Fix <a href="https://github.com/Nicd/code-stats/issues/5">#5</a>: Add link to example stats to front page),
         ~E(Fix <a href="https://github.com/Nicd/code-stats/issues/6">#6</a>: Add clarification about recent XP)
       ]},
      {"1.2.0", ~D[2016-05-31], "Scaling back levels",
       [
         "Scaled back the level algorithm to grant less levels. XP is not affected. The aim is to prevent an inflation of levels. The level algorithm may be modified further later on.",
         "Combined the source and changes page."
       ]},
      {"1.1.1", ~D[2016-05-30], "Clock drift",
       [
         "Fixed a case where clock drift either on the client or the server caused pulses to not be accepted."
       ]},
      {"1.1.0", ~D[2016-05-30], "First public release",
       [
         ~E"Implemented <em>everything</em>. 🎉"
       ]}
    ]
  end
end
