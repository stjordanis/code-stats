import { el, list } from 'redom';

import { INT_FORMATTER, format_mins } from '../../../common/utils';
import { XP_FORMATTER } from '../../../common/xp_utils';

const MAX_LANGS = 10;

class TopFlowLanguageInfoComponent {
  constructor(langDataSource) {
    this.langDataSource = langDataSource;

    this.language = null;
    this.amount = 0;
    this.totalXp = 0;
    this.totalDuration = 0;
    this.averageXp = 0;
    this.averageDuration = 0;
    this.flowScore = 0;

    this.languageEl = el('h5.top-flow-language-name');

    this.flowEl = el('p.top-flow-language-flow-score');
    this.flowOverlayEl = this.flowEl.cloneNode();
    this.flowOverlayEl.classList.add('top-flow-language-flow-score-overlay');
    this.flowOverlayEl.style.height = '100%';
    this.flowEl.classList.add('top-flow-language-flow-score-base');

    this.amountEl = el('p.top-flow-language-amount');
    this.xpEl = el('p.top-flow-language-total-xp');
    this.durationEl = el('p.top-flow-language-duration');

    this.el = el('tr.top-flow-language', [
      el('td.top-flow-language-flow-score-td', [this.flowEl, this.flowOverlayEl]),
      el('td', [this.languageEl]),
      el('td', [this.amountEl]),
      el('td', [this.xpEl]),
      el('td', [this.durationEl]),
    ]);
  }

  update({ language, amount, averageDuration, averageXp, totalDuration, totalXp }) {
    this.language = language;
    this.amount = amount;
    this.totalDuration = totalDuration;
    this.totalXp = totalXp;
    this.averageDuration = averageDuration;
    this.averageXp = averageXp;

    const langData = this.langDataSource.getLangByName(this.language);
    this.flowScore = Math.round(this.totalXp / langData.xp * 100);

    this.languageEl.textContent = language;

    this.flowEl.textContent = INT_FORMATTER.format(this.flowScore);
    this.flowOverlayEl.textContent = this.flowEl.textContent;
    this.flowEl.setAttribute('aria-label', `Flow score: ${this.flowEl.textContent}`);
    this.flowOverlayEl.setAttribute('aria-label', this.flowEl.getAttribute('aria-label'));
    this.flowOverlayEl.style.height = `${100 - INT_FORMATTER.format(this.flowScore)}%`;

    this.amountEl.textContent = this.getAmountStr();
    this.xpEl.textContent = this.getXpStr();
    this.durationEl.textContent = this.getDurationStr();
  }

  getAmountStr() {
    return `${INT_FORMATTER.format(this.amount)} flows`;
  }

  getXpStr() {
    return `${XP_FORMATTER.format(this.totalXp)} XP, avg ${XP_FORMATTER.format(this.averageXp)} XP`;
  }

  getDurationStr() {
    return `${format_mins(this.totalDuration)}, avg ${format_mins(this.averageDuration)}`
  }

  static getKey(item) {
    return item.language;
  }
}

class TopFlowLanguagesComponent {
  constructor(langDataSource) {
    this.heading = el('h4.top-flow-languages-heading', 'Top flow languages');
    this.datalist = list(
      'table.top-flow-languages-datalist',
      TopFlowLanguageInfoComponent,
      TopFlowLanguageInfoComponent.getKey,
      langDataSource
    );

    this.el = el('section.top-flow-languages', [
      this.heading,
      this.datalist
    ]);
  }

  setInitData({ top_flow_languages }) {
    let entries = Object.entries(top_flow_languages);
    entries = entries.sort(([_k1, v1], [_k2, v2]) => v2.amount - v1.amount);
    entries = entries.slice(0, MAX_LANGS);
    entries = entries.map(([language, data]) => ({ ...data, language }));

    this.datalist.update(entries);
  }

  update() {

  }
}

export { TopFlowLanguagesComponent };
