defmodule CodeStats.Repo.Migrations.RemovePulseUpdated do
  use Ecto.Migration

  def change do
    alter table(:pulses) do
      remove(:updated_at, :utc_datetime, default: DateTime.utc_now())
    end
  end
end
