defmodule CodeStats.User.Alias do
  @moduledoc """
  A user specific language alias, can override global aliases defined in languages table.
  """

  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query, only: [from: 2]

  alias CodeStats.Language

  @primary_key false

  @type t :: %__MODULE__{
          source_id: pos_integer(),
          target_id: pos_integer() | nil
        }

  defmodule WithLanguages do
    @moduledoc """
    User alias with preloaded languages.
    """

    import CodeStats.Utils.TypedStruct

    deftypedstruct(%{
      source_id: pos_integer(),
      target_id: pos_integer() | nil,
      source: Language.t(),
      target: Language.t() | nil
    })
  end

  embedded_schema do
    field(:source_id, :integer)
    field(:target_id, :integer)

    # These are only used for the changeset
    field(:source, :string, virtual: true)
    field(:target, :string, virtual: true)
  end

  def changeset(data, params) do
    data
    |> cast(params, [:source, :target])
    |> validate_required([:source])
    |> validate_not_same()
    |> transform_change()
  end

  @spec preload_languages([t()]) :: [WithLanguages.t()]
  @doc """
  Preload language structs for the given aliases, similarly to how Ecto does it.

  If any languages cannot be found, those aliases are filtered out.
  """
  def preload_languages(aliases) do
    ids =
      Enum.reduce(aliases, MapSet.new(), fn alias, acc ->
        acc = MapSet.put(acc, alias.source_id)

        if not is_nil(alias.target_id) do
          MapSet.put(acc, alias.target_id)
        else
          acc
        end
      end)
      |> MapSet.to_list()

    langs =
      from(l in Language, where: l.id in ^ids, select: {l.id, l})
      |> CodeStats.Repo.all()
      |> Map.new()

    Enum.reduce(aliases, [], fn alias, acc ->
      source = Map.get(langs, alias.source_id)
      target = Map.get(langs, alias.target_id)

      # Filter out any aliases that point to nonexistent languages
      if is_nil(source) do
        acc
      else
        [
          %WithLanguages{
            source_id: alias.source_id,
            target_id: alias.target_id,
            source: source,
            target: target
          }
          | acc
        ]
      end
    end)
    |> Enum.reverse()
  end

  @spec validate_not_same(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp validate_not_same(changeset)

  defp validate_not_same(%Ecto.Changeset{valid?: true} = changeset) do
    source = get_change(changeset, :source) |> Language.sanitize_language()

    target =
      get_change(changeset, :target)
      |> case do
        nil -> nil
        name -> Language.sanitize_language(name)
      end

    changeset = changeset |> put_change(:source, source) |> put_change(:target, target)

    if source == target do
      add_error(changeset, :source, "Source language cannot be the same as target language.")
    else
      changeset
    end
  end

  defp validate_not_same(changeset), do: changeset

  @spec transform_change(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp transform_change(changeset)

  defp transform_change(%Ecto.Changeset{valid?: true} = changeset) do
    source_res = get_change(changeset, :source) |> Language.get_or_create()

    target_res =
      get_change(changeset, :target)
      |> case do
        nil -> {:ok, nil}
        name -> Language.get_or_create(name)
      end

    cond do
      source_res == {:error, :unknown} ->
        add_error(changeset, :source, "Source language could not be created.")

      target_res == {:error, :unknown} ->
        add_error(changeset, :target, "Target language could not be created.")

      true ->
        {:ok, source} = source_res
        {:ok, target} = target_res

        changeset = put_change(changeset, :source_id, source.id)

        if not is_nil(target) do
          put_change(changeset, :target_id, target.id)
        else
          put_change(changeset, :target_id, nil)
        end
    end
  end

  defp transform_change(changeset), do: changeset
end
