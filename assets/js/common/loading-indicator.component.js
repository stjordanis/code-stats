import { el } from 'redom';

const S = Math.sin;
const C = Math.cos;
const T = Math.tan;

function R(r, g, b, a) {
  a = a === undefined ? 1 : a;
  return "rgba(" + (r | 0) + "," + (g | 0) + "," + (b | 0) + "," + a + ")";
}

// Canvas
let c = null;

// Canvas 2D context
let x = null;

/**
 * Loading indicators from Dwitter.
 *
 * NOTE: These indicators are used with the permission of their respective owners. THEY DO NOT FALL UNDER THE LICENSE
 * OF THE MAIN CODEBASE AND THEY REMAIN COPYRIGHTED TO THEIR CREATORS.
 */
const INDICATORS = [
  {
    author: 'Xen',
    url: '6076',
    f: t => { let X, Y; x.beginPath(X = 1e3 - C(t / 5) * 799, Y = 500 + S(t + S(t / 3)) * 299), x.fill(x.arc(X, Y, 99, 0, 7)), x.arc(X, Y, 99, 0, 7), x.stroke(), x.fillStyle = `hsl(${t * 9},90%,30%)`; }
  },
  {
    author: 'Xen',
    url: '5514',
    f: t => {
      let X, Y, Z;
      X = (C(t ** 9) + 1) / 2;
      Y = Math.random();
      x.fillRect(X * 900, t * 99 % 1e3, Z = 29, Z);
      x.fillRect(1920 - Y * 900, t * 99 % 1e3, Z, Z);
      x.fillStyle = `hsla(${t * Z},99%,40%,.5)`;
    }
  },
  {
    author: 'Xen',
    url: '4723',
    f: t => {
      let W, H;
      x.strokeRect(3, 4, W = 1912, H = 1072);
      x.drawImage(c, 4, 4, W, H);
      x.strokeStyle = `hsl(${t * H / 9},99%,50%)`;
      x.rotate(C(t) / 1e4);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '5112',
    f: t => { let w, h, z; w = 1920 / 2; h = 1080 / 2; if ((t * t) / S(t * t) > 1) { z = (t * t) / S(t * t); } x.strokeStyle = R(15, 25, 15, 0.75); x.beginPath(); x.arc(w, h, z, 0, Math.PI / S(t)); x.stroke(); }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '5118',
    f: t => {
      let w, h, z;
      w = 1920 / 2;
      h = 1080 / 2;
      z = t * t;
      x.strokeStyle = `hsl(${t * 90},50%,50%)`;
      x.beginPath();
      x.arc(w, h, z, 0, Math.PI * 2);
      x.stroke();
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '5121',
    f: t => { let w, h, z; w = 1920 / 2; h = 1080 / 2; z = t * t; x.beginPath(); x.arc(w, h, z, 0, Math.PI * 2); x.rotate(20, 0, 0); x.strokeStyle = `hsl(${t * 90},50%,50%)`; x.stroke(); }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '6293',
    f: t => {
      x.fillStyle = `hsl(${t * 90},50%,50%)`;
      x.fillRect(940 + S(t % 3) * 300, 550 + C(t) * 290, 20, 20);
      x.fillRect(940 + S(-t % 3) * 300, 550 + C(t) * 290, 20, 20);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '6706',
    f: t => {
      let a, b, d;
      for (c.width = a = 2e3; a--; x.beginPath(x.stroke()))b = 255, d = C(a) * 20, x.strokeStyle = R(b * S(a) + b, 25 * d, 99 + d, .5), x.arc(960 + b * C(a + t), 540 + b * C(a), 70 + d, 0, 7);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '10259',
    f: t => {
      x.beginPath(x.fillStyle = '#fff'), x.shadowColor = `hsl(${t * 90} 99%50%)`, x.shadowBlur = 25, x.ellipse(960, 540, (S(t) * 20) ** 2, (C(t) * 20) ** 2, 0, 0, 7, 0), x.fill();
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '9895',
    f: t => {
      let g;
      c.style.background = R(); x.fillRect(100 - C(t) * 250 + (t * 70), 440 + S(t) * 250, g = S(t) * 50 + 65, g + 1.6), x.shadowBlur = 9, x.shadowColor = `hsl(${t * 250},99%,70%)`;
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '9882',
    f: t => {
      let a;
      x.fillRect(t * 100, a = 540 - (~~(S(t) * 2.5 + 2.5)) * 50, 15, 15), x.fillStyle = `hsl(${a ** 2 % 360},50%,50%)`;
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '8528',
    f: t => {
      let a, i, π, n;
      for (i = 2; i--;)x.beginPath(x.fillStyle = R(i ? a = S(t) * 129 + 129 : 0, 0, i ? 0 : a)) | x.arc(960 + 80 * S(i ? π = t * 5 : n), 540 + 80 * C(i ? n = π + 3 : π), 80 - 30 * S(t * 8), 0, 7) | x.fill();
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '8369',
    f: t => {
      let i, W;
      for (c.width |= i = 1e3; i--;)x.globalAlpha = .1, x.fillRect(S(t * 2 + i / 50) * i + 960, C(t * 3 + i / 50) * i + 540, W = i / 9 + 2, W);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '8354',
    f: t => {
      let i;
      c.width |= i = 1e3
      for (; i--; x.fillStyle = `hsl(${i % 360},50%,${i / 20 + 50}%)`)x.fillRect(i * 2, S(t + i * 1.5) * 100 + 500, 9, 9);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '7852',
    f: t => {
      let i;
      c.width = 1920;
      for (i = 0; i < ~~t % 8 + 1; i++)x.fillRect(400 + i * 100 + S(t) * 300, 400, 50, 200);
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '7540',
    f: t => {
      let i, a;
      x.rotate(c.width ^= 0.5, a = 9e3)
      for (i = 1e3; i--;)x.fillRect(50 * i + S(t / 100) * a - a, -a / 3, 25, a), x.fillStyle = `hsl(${t * 9 + (i * 20)},50%,50%)`;
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '7208',
    f: t => {
      let q, X, Y;
      q = 30; for (X = 64; X--;)for (Y = 32; Y--; x.fillRect(X * q, Y * q, q, q))x.fillStyle = `hsl(${t * 90 + Y * q},50%,50%)`;
    }
  },
  {
    author: 'BirdsTweetCodersDweet',
    url: '7042',
    f: t => {
      let w, m, i, k;
      c.width = w = 1920; m = 150; for (i = 0; i++ < m;)k = C(t * 2 + i / 50) * 255, x.fillStyle = R(255 - k / 2, 0, k), x.fillRect(i / m * w, w / 4 + 100 * S(i / 10 + t / 2), 10, m * C(i + t));
    }
  },
  {
    author: 'Xen',
    url: '16490',
    f: t => {
      let i;
      for (i = 2e3; i--; x.fillRect(i, 0, 2, 9))x.fillStyle = `hsl(${i / 5 + C(i + t * 49) * 99},69%,50%)`; x.drawImage(c, 0, 9);
    }
  },
  {
    author: 'Xen',
    url: '20202',
    f: t => {
      x.fillRect(940 + t * C(t) * 9, 540 + t * S(t) * 9, t, t);
    }
  },
  {
    author: 'Xen',
    url: '19859',
    f: t => {
      let z;
      x.fillStyle = `hsl(${z = t * 2e3} 50%50%)`;
      x.fillRect(z % 2e3, z / 1e3 * 4, 39, 5);
    }
  },
];

class LoadingIndicatorComponent {
  constructor(msg = 'Loading data…') {
    let i = Math.floor(Math.random() * INDICATORS.length);
    let url = new URL(window.location.href);
    if (url.searchParams.has("d")) {
      i = INDICATORS.findIndex(indicator => indicator.url === url.searchParams.get("d"));
    }

    this.indicator = INDICATORS[i];

    this.canvas = c = el('canvas#loading-profile', { width: 1920, height: 1080 });
    this.el = el('div.loading-indicator', [
      el('div.canvas-wrapper', [this.canvas]),
      el('span.loading-text', msg),
      el('span.loading-author', [
        el(
          'a',
          {
            href: `https://www.dwitter.net/d/${this.indicator.url}`,
            target: '_blank'
          },
          `Animation © ${this.indicator.author}`)
      ])
    ]);

    this.running = true;

    this.initIndicator();
  }

  initIndicator() {
    x = this.canvas.getContext("2d");
    let time = 0;
    let frame = 0;
    const u = t => this.indicator.f(t);

    const loop = () => {
      // Stop if component was removed from page
      if (!this.running) return;

      time = frame / 60;

      if (time * 60 | 0 == frame - 1) {
        time += 0.000001;
      }

      ++frame;

      try {
        u(time);
      } catch (e) {
        console.error('Error in loading indicator', e);
        return;
      }

      requestAnimationFrame(loop);
    };

    requestAnimationFrame(loop);
  }

  onunmount() {
    this.running = false;
  }
}

export default LoadingIndicatorComponent;
